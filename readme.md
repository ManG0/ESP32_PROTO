# Software

1. VS CODE
2. Platformio

# Hilfreiche Doku

https://docs.platformio.org/en/latest/boards/espressif32/esp32thing_plus.html

https://learn.sparkfun.com/tutorials/esp32-thing-plus-hookup-guide?_ga=2.199991723.460321622.1623352854-1978485539.1619546023#introduction

# Treiber

CP2104 USB Driver
https://www.silabs.com/developers/usb-to-uart-bridge-vcp-drivers

# Nice To Know

Wenn er keine Verbindung fürs Uploaden herstellen kann den Reset Knopf drücken auf dem Board. Passiert des öfteren wenn er zu schnell startet.
